package de.dhbw.foodcoops;

import java.util.ArrayList;
import java.util.List;
import net.java.html.json.*;

import de.dhbw.foodcoops.js.PlatformServices;

@Model(className = "Data", properties = {
		@Property(name = "message", type = String.class),
		@Property(name = "rotating", type = boolean.class)
})
public class Data
{
	private String message = "";
	private boolean rotating = false;

	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public boolean isRotating()
	{
		return rotating;
	}
	public void setRotating(boolean rotating)
	{
		this.rotating = rotating;
	}

	public List<String> getWords()
	{
		String[] words = this.message.split(" ");
		List<String> wordList = new ArrayList<>();
		int numberOfElements = words.length > 6 ? words.length : 6;

		for(int i = 0; i < numberOfElements; i++)
		{
			if(i > words.length-1)
			{
				wordList.add("Filler: " + i);
			}
			else
			{
				wordList.add(words[i]);
			}
		}

		return wordList;
	}

	public void initServices(PlatformServices services)
	{
		// I don't know what to do here yet
	}

	public void applyBindings()
	{
		// I don't know what to do here yet
	}
}
